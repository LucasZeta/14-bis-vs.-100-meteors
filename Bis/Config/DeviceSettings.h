//
//  DeviceSettings.h
//  Bis
//
//  Created by Lucas de Souza da Conceição on 24/08/13.
//  Copyright (c) 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Assets.h"

@interface DeviceSettings : NSObject

#define SCREEN_WIDTH() [CCDirector sharedDirector].winSize.width
#define SCREEN_HEIGHT() [CCDirector sharedDirector].winSize.height
#define WIN_SIZE() [CCDirector sharedDirector].winSize

@end
