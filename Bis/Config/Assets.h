//
//  Assets.h
//  Bis
//
//  Created by Lucas de Souza da Conceição on 24/08/13.
//  Copyright (c) 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Assets : NSObject

#define kBACKGROUND @"background.png"
#define kPLAY @"play-button.png"
#define kHIGHSCORE @"highscore-button.png"
#define kHELP @"help-button.png"
#define kSOUND @"sound-button.png"
#define kMETEOR @"meteor.png"
#define kSHIP @"14bis.png"
#define kLEFT @"left-button.png"
#define kRIGHT @"right-button.png"
#define kSHOOT @"shoot-button.png"
#define kSHOT @"shot.png"
#define kGAMEOVER @"gameover.png"
#define kGAMEWON @"finalend.png"

#define COLLISION_SOUND @"bang.wav"
#define SHOT_SOUND @"shoot.wav"
#define PLAYER_HIT_SOUND @"over.wav"
#define BACKGROUND_SOUND @"music.wav"

@end
