//
//  Animations.m
//  Bis
//
//  Created by Lucas de Souza da Conceição on 07/10/13.
//  Copyright (c) 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import "Animations.h"

@implementation Animations

+(Animations *)instance
{
    static Animations *instance;

    if (! instance) {
        instance = [[Animations alloc] init];
    }

    return instance;
}

-(void)shrink:(CCSprite *)sprite andFadeOutIn:(float)seconds withCallback:(SEL)callback
{
    CCScaleBy *animation1 = [CCScaleBy actionWithDuration:seconds scale:0.5f];
    CCFadeOut *animation2 = [CCFadeOut actionWithDuration:seconds];
    CCSpawn *spawn = [CCSpawn actionWithArray:[NSArray arrayWithObjects:animation1, animation2, nil]];

    NSMutableArray *actions = [NSMutableArray arrayWithObjects:spawn, nil];

    if (callback) {
        CCCallFunc *function = [CCCallFunc actionWithTarget:sprite selector:callback];

        [actions addObject:function];
    }

    [sprite runAction:[CCSequence actionWithArray:actions]];
}

-(void)shrink:(CCSprite *)sprite andFadeOutIn:(float)seconds
{
    [self shrink:sprite andFadeOutIn:seconds withCallback:nil];
}

@end
