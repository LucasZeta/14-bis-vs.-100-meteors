//
//  Animations.h
//  Bis
//
//  Created by Lucas de Souza da Conceição on 07/10/13.
//  Copyright (c) 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Animations : NSObject

+(Animations *)instance;

-(void)shrink:(CCSprite *)sprite andFadeOutIn:(float)seconds withCallback:(SEL)callback;
-(void)shrink:(CCSprite *)sprite andFadeOutIn:(float)seconds;

@end
