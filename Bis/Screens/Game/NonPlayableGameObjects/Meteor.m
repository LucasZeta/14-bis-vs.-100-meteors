//
//  Meteor.m
//  Bis
//
//  Created by Lucas de Souza da Conceição on 28/08/13.
//  Copyright (c) 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import "Meteor.h"

@implementation Meteor

+(Meteor *)meteor
{
    Meteor *meteor = [Meteor spriteWithFile:kMETEOR];

    meteor.step = -1;
    meteor.positionX = arc4random_uniform(SCREEN_WIDTH());
    meteor.positionY = SCREEN_HEIGHT();
    meteor.explosionSound = COLLISION_SOUND;

    [meteor configure];

    return meteor;
}

-(void)remove
{
    if ([self.delegate respondsToSelector:@selector(meteorWillBeRemoved:)]) {
        [self.delegate meteorWillBeRemoved:self];
    }

    [super remove];
}

@end
