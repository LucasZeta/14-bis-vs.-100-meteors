//
//  Score.m
//  Bis
//
//  Created by Lucas de Souza da Conceição on 18/10/13.
//  Copyright (c) 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import "Score.h"

@implementation Score

+(Score *)score
{
    return [[[Score alloc] init] autorelease];
}

-(id)init
{
    self = [super init];

    if (self) {
        self.position = ccp(SCREEN_WIDTH() - 50.0f, SCREEN_HEIGHT() - 50.0f);
        self.score = 0;

        self.scoreLabel = [CCLabelBMFont labelWithString:@"" fntFile:@"UniSansSemiBold_Numbers_240.fnt"];
        self.scoreLabel.scale = 1.0f;
        [self updateLabel];

        [self addChild:self.scoreLabel];
    }

    return self;
}

-(void)increase
{
    self.score++;
    [self updateLabel];
}

-(void)updateLabel
{
    self.scoreLabel.string = [NSString stringWithFormat:@"%d", self.score];
}

@end