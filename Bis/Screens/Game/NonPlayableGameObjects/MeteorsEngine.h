//
//  MeteorsEngine.h
//  Bis
//
//  Created by Lucas de Souza da Conceição on 28/08/13.
//  Copyright (c) 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import "CCLayer.h"
#import "Meteor.h"

@protocol MeteorsEngineDelegate;

@interface MeteorsEngine : CCLayer

@property (nonatomic, assign) id<MeteorsEngineDelegate>delegate;

+(MeteorsEngine *)meteorsEngine;

@end

@protocol MeteorsEngineDelegate <NSObject>

-(void)meteorsEngineDelegateDidCreateMeteor:(Meteor *)meteor;

@end