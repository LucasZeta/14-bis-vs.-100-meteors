//
//  MeteorsEngine.m
//  Bis
//
//  Created by Lucas de Souza da Conceição on 28/08/13.
//  Copyright (c) 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import "MeteorsEngine.h"

@implementation MeteorsEngine

+(MeteorsEngine *)meteorsEngine
{
    return [[[MeteorsEngine alloc] init] autorelease];
}

-(id)init
{
    self = [super init];
    
    if (self) {
        [self schedule:@selector(meteorsEngine:) interval:(1.0f / 10.0f)];
    }
    
    return self;
}

-(void)meteorsEngine:(float)dt
{
    if (arc4random_uniform(30) == 0) {
        if ([self.delegate respondsToSelector:@selector(meteorsEngineDelegateDidCreateMeteor:)]) {
            [self.delegate meteorsEngineDelegateDidCreateMeteor:[Meteor meteor]];
        }
    }
}

@end
