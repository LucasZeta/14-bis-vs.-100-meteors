//
//  Score.h
//  Bis
//
//  Created by Lucas de Souza da Conceição on 18/10/13.
//  Copyright (c) 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import "CCLayer.h"

@interface Score : CCLayer

@property (nonatomic, assign) int score;
@property (nonatomic, assign) CCLabelBMFont *scoreLabel;

+(Score *)score;

-(void)increase;

@end
