//
//  SingleDirectionMovement.h
//  Bis
//
//  Created by Lucas de Souza da Conceição on 20/10/13.
//  Copyright (c) 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import "CCSprite.h"
#import "Animations.h"

@interface SingleDirectionMovement : CCSprite

@property (nonatomic, assign) float positionX;
@property (nonatomic, assign) float positionY;
@property (nonatomic, assign) int step;
@property (nonatomic, assign) BOOL isBeingRemoved;
@property (nonatomic, assign) NSString *spawnSound;
@property (nonatomic, assign) NSString *explosionSound;

-(void)configure;
-(void)start;
-(void)update:(float)dt;
-(void)explode;
-(void)remove;

@end
