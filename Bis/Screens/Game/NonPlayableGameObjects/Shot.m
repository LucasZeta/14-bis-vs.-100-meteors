//
//  Shot.m
//  Bis
//
//  Created by Lucas de Souza da Conceição on 25/09/13.
//  Copyright (c) 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import "Shot.h"

@implementation Shot

+(Shot *)shootWithPositionX:(float)positionX andPositionY:(float)positionY
{
    Shot *shot = [Shot spriteWithFile:kSHOT];

    shot.step = 2;
    shot.positionX = positionX;
    shot.positionY = positionY;
    shot.spawnSound = SHOT_SOUND;

    [shot configure];

    return shot;
}

-(void)remove
{
    if ([self.delegate respondsToSelector:@selector(shotWillBeRemoved:)]) {
        [self.delegate shotWillBeRemoved:self];
    }

    [super remove];
}

@end
