//
//  SingleDirectionMovement.m
//  Bis
//
//  Created by Lucas de Souza da Conceição on 20/10/13.
//  Copyright (c) 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import "SingleDirectionMovement.h"

@implementation SingleDirectionMovement

-(void)configure
{
    self.isBeingRemoved = NO;
}

-(void)start
{
    [self playSound:self.spawnSound];
    [self scheduleUpdate];
}

-(void)move
{
    self.position = ccp(self.positionX, self.positionY);
}

-(void)update:(float)dt
{
    self.positionY += self.step;

    [self move];
}

-(void)explode
{
    [self playSound:self.explosionSound];

    self.isBeingRemoved = YES;

    [self unscheduleUpdate];

    [[Animations instance] shrink:self andFadeOutIn:0.2f withCallback:@selector(remove)];
}

-(void)remove
{
    [self removeFromParentAndCleanup:YES];
}

-(void)playSound:(NSString *)sound
{
    if (sound) {
        [[SimpleAudioEngine sharedEngine] playEffect:sound];
    }
}

@end
