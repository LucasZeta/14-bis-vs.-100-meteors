//
//  Shot.h
//  Bis
//
//  Created by Lucas de Souza da Conceição on 25/09/13.
//  Copyright (c) 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import "SingleDirectionMovement.h"

@protocol ShotDelegate;

@interface Shot : SingleDirectionMovement

@property (nonatomic, assign) id<ShotDelegate>delegate;

+(Shot *)shootWithPositionX:(float)positionX andPositionY:(float)positionY;

@end

@protocol ShotDelegate <NSObject>

-(void)shotWillBeRemoved:(Shot *)shot;

@end
