//
//  Meteor.h
//  Bis
//
//  Created by Lucas de Souza da Conceição on 28/08/13.
//  Copyright (c) 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import "SingleDirectionMovement.h"

@protocol MeteorDelegate;

@interface Meteor : SingleDirectionMovement

@property (nonatomic, assign) id<MeteorDelegate>delegate;

+(Meteor *)meteor;

@end

@protocol MeteorDelegate <NSObject>

-(void)meteorWillBeRemoved:(Meteor *)meteor;

@end