//
//  GameScreen.m
//  Bis
//
//  Created by Lucas de Souza da Conceição on 28/08/13.
//  Copyright (c) 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import "GameScreen.h"

@implementation GameScreen

+(CCScene *)scene
{
    CCScene *scene = [CCScene node];
    GameScreen *layer = [GameScreen node];
    
    [scene addChild:layer];
    
    return scene;
}

-(id)init
{
    self = [super init];

    if (self) {
        [self addBackground];
        [self initGameObjects];

        [self preloadCache];
    }

    return self;
}

-(void)initGameObjects
{
    [self initMeteors];
    [self initPlayer];
    [self initPlayerButtons];
    [self initShots];
    [self initScore];
}

-(void)initMeteors
{
    self.meteorsList = [NSMutableArray array];
    self.meteorsEngine = [MeteorsEngine meteorsEngine];

    [self addMeteorsLayer];
}

-(void)initPlayer
{
    self.playerLayer = [CCLayer node];

    self.player = [Player player];
    self.player.delegate = self;

    [self.playerLayer addChild:self.player];
    [self addChild:self.playerLayer];

    self.playersList = [NSMutableArray array];
    [self.playersList addObject:self.player];
}

-(void)initPlayerButtons
{
    float buttonsHeight = (SCREEN_HEIGHT() / -2.0f) + 50.0f;

    CCMenu *menu = [CCMenu menuWithItems:
        [self addButton:kLEFT :@selector(moveLeft:) :-110.0f :buttonsHeight],
        [self addButton:kRIGHT :@selector(moveRight:) :-50.0f :buttonsHeight],
        [self addButton:kSHOOT :@selector(shoot:) :(SCREEN_WIDTH() / 2.0f) - 50.0f :buttonsHeight],
        nil
    ];

    self.playerButtonsLayer = [CCLayer node];
    [self.playerButtonsLayer addChild:menu];

    [self addChild:self.playerButtonsLayer];
}

-(CCMenuItemGameButton *)addButton:(NSString *)spriteFile :(SEL)selector :(float)positionX :(float)positionY
{
    CCSprite *sprite = [CCSprite spriteWithFile:spriteFile];
    CCSprite *sprite2 = [CCSprite spriteWithFile:spriteFile];

    CCMenuItemGameButton *item = [CCMenuItemGameButton
        itemWithNormalSprite:sprite
        selectedSprite:sprite2
        target:self
        selector:selector
    ];

    item.position = ccp(positionX, positionY);

    return item;
}

-(void)initShots
{
    self.shotsLayer = [CCLayer node];
    [self addChild:self.shotsLayer];

    self.shotsList = [NSMutableArray array];
}

-(void)initScore
{
    self.scoreLayer = [CCLayer node];
    [self addChild:self.scoreLayer];

    self.score = [Score score];
    [self.scoreLayer addChild:self.score];
}

-(void)onEnter
{
    [super onEnter];

    [self startGame];
}

-(void)startGame
{
    [self addChild:self.meteorsEngine];
    self.meteorsEngine.delegate = self;
    
    [self schedule:@selector(checkHits)];
}

-(void)addBackground
{
    CCSprite *background = [CCSprite spriteWithFile:kBACKGROUND];
    background.position = ccp(SCREEN_WIDTH() / 2.0f, SCREEN_HEIGHT() / 2.0f);

    [self addChild:background];
    [[SimpleAudioEngine sharedEngine] playBackgroundMusic:BACKGROUND_SOUND loop:YES];
}

-(void)addMeteorsLayer
{
    self.meteorsLayer = [CCLayer node];
    [self addChild:self.meteorsLayer];
}

-(void)meteorsEngineDelegateDidCreateMeteor:(Meteor *)meteor
{
    [self.meteorsLayer addChild:meteor];
    meteor.delegate = self;
    [meteor start];
    [self.meteorsList addObject:meteor];
}

-(void)dealloc
{
    [_meteorsEngine release];
    [_meteorsList release];

    [super dealloc];
}

-(void)moveLeft:(id)sender
{
    [self.player moveLeft];
}

-(void)moveRight:(id)sender
{
    [self.player moveRight];
}

-(void)shoot:(id)sender
{
    [self.player shoot];
}

-(void)playerDidShot:(Shot *)shot
{
    [self.shotsLayer addChild:shot];
    shot.delegate = self;
    [shot start];
    [self.shotsList addObject:shot];
}

-(void)checkRadiusHitsOfArray:(NSMutableArray *)array1 againstArray:(NSMutableArray *)array2 withSender:(id)sender andCallbackMethod:(SEL)callback
{
    for (CCSprite *obj1 in array1) {
        CGRect box1 = obj1.boundingBox;

        for (CCSprite *obj2 in array2) {
            CGRect box2 = obj2.boundingBox;

            if (CGRectIntersectsRect(box1, box2)) {
                if ([sender respondsToSelector:callback]) {
                    [sender performSelector:callback withObject:obj1 withObject:obj2];
                }
            }
        }
    }
}

-(void)checkHits
{
    [self checkRadiusHitsOfArray:self.playersList againstArray:self.meteorsList withSender:self andCallbackMethod:@selector(playerWasHit:byMeteor:)];

    [self checkRadiusHitsOfArray:self.meteorsList againstArray:self.shotsList withSender:self andCallbackMethod:@selector(meteorHit:withShot:)];
}

-(void)playerWasHit:(id)player byMeteor:(id)meteor
{
    [(Player *)player explode];
    [(Meteor *)meteor explode];

    [self gameOver];
}

-(void)meteorHit:(id)meteor withShot:(id)shot
{
    if ([meteor isKindOfClass:[Meteor class]] && [shot isKindOfClass:[Shot class]]) {
        if ((! [(Shot *)shot isBeingRemoved]) && (! [(Meteor *)meteor isBeingRemoved])) {
            [(Meteor *)meteor explode];
            [(Shot *)shot explode];

            [self.score increase];

            if (self.score.score >= 10) {
                [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[EndingScreen goodScene]]];
            }
        }
    }
}

-(void)meteorWillBeRemoved:(Meteor *)meteor
{
    meteor.delegate = nil;
    [self.meteorsList removeObject:meteor];
}

-(void)shotWillBeRemoved:(Shot *)shot
{
    shot.delegate = nil;
    [self.shotsList removeObject:shot];
}

-(void)preloadCache
{
    [[SimpleAudioEngine sharedEngine] preloadEffect:COLLISION_SOUND];
    [[SimpleAudioEngine sharedEngine] preloadEffect:SHOT_SOUND];
    [[SimpleAudioEngine sharedEngine] preloadEffect:PLAYER_HIT_SOUND];
    [[SimpleAudioEngine sharedEngine] preloadEffect:BACKGROUND_SOUND];
}

-(void)gameOver
{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[EndingScreen badScene]]];
}

@end