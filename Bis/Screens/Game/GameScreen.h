//
//  GameScreen.h
//  Bis
//
//  Created by Lucas de Souza da Conceição on 28/08/13.
//  Copyright (c) 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import "CCLayer.h"
#import "MeteorsEngine.h"
#import "Player.h"
#import "CCMenuItemGameButton.h"
#import "Shot.h"
#import "Score.h"
#import "EndingScreen.h"

@interface GameScreen : CCLayer <MeteorsEngineDelegate, PlayerDelegate, MeteorDelegate, ShotDelegate>

@property (nonatomic, retain) MeteorsEngine *meteorsEngine;
@property (nonatomic, retain) NSMutableArray *meteorsList;
@property (nonatomic, retain) CCLayer *meteorsLayer;

@property (nonatomic, retain) CCLayer *playerLayer;
@property (nonatomic, retain) Player *player;
@property (nonatomic, retain) NSMutableArray *playersList;

@property (nonatomic, retain) CCLayer *playerButtonsLayer;

@property (nonatomic, retain) CCLayer *shotsLayer;
@property (nonatomic, retain) NSMutableArray *shotsList;

@property (nonatomic, retain) CCLayer *scoreLayer;
@property (nonatomic, retain) Score *score;

+(CCScene *) scene;

@end
