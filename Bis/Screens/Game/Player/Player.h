//
//  Player.h
//  Bis
//
//  Created by Lucas de Souza da Conceição on 22/09/13.
//  Copyright (c) 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import "CCSprite.h"
#import "Shot.h"

@protocol PlayerDelegate;

@interface Player : CCSprite

@property (nonatomic, assign) id<PlayerDelegate>delegate;

@property (nonatomic, assign) float positionX;
@property (nonatomic, assign) float positionY;

+(Player *)player;

-(void)shoot;
-(void)moveLeft;
-(void)moveRight;
-(void)explode;

@end

@protocol PlayerDelegate <NSObject>

-(void)playerDidShot:(Shot *)shot;

@end
