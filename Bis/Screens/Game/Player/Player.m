//
//  Player.m
//  Bis
//
//  Created by Lucas de Souza da Conceição on 22/09/13.
//  Copyright (c) 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import "Player.h"
#import "Animations.h"

@implementation Player

+(Player *)player
{
    Player *player = [Player spriteWithFile:kSHIP];

    [player initPlayerPosition];
    [player movePlayer];

    return player;
}

-(void)initPlayerPosition
{
    self.positionX = SCREEN_WIDTH() / 2.0f;
    self.positionY = 120.0f;
}

-(void)movePlayer
{
    self.position = ccp(self.positionX, self.positionY);
}

-(void)moveLeft
{
    if (self.positionX > 30.0f) {
        self.positionX -= 10.0f;
        [self movePlayer];
    }
}

-(void)moveRight
{
    if (self.positionX < SCREEN_WIDTH() - 30.0f) {
        self.positionX += 10.0f;
        [self movePlayer];
    }
}

-(void)shoot
{
    if ([self.delegate respondsToSelector:@selector(playerDidShot:)]) {
        [self.delegate playerDidShot:[Shot shootWithPositionX:self.positionX andPositionY:self.positionY]];
    }
}

-(void)explode
{
    [[SimpleAudioEngine sharedEngine] playEffect:PLAYER_HIT_SOUND];
    [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];

    [[Animations instance] shrink:self andFadeOutIn:0.5f];
}

@end
