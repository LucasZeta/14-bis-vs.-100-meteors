//
//  TitleScreen.m
//  Bis
//
//  Created by Lucas de Souza da Conceição on 24/08/13.
//  Copyright 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import "TitleScreen.h"
#import "AppDelegate.h"
#import "GameScreen.h"

@implementation TitleScreen

+(CCScene*)scene
{
    CCScene *scene = [CCScene node];
    TitleScreen *layer = [TitleScreen node];
    
    [scene addChild:layer];
    
    return scene;
}

-(id)init
{
    self = [super init];
    
    if (self) {
        CCSprite *background = [CCSprite spriteWithFile:kBACKGROUND];
        background.position = ccp(SCREEN_WIDTH() / 2.0f, SCREEN_HEIGHT() / 2.0f);

        [self addChild:background];

        CCMenu *menu = [CCMenu menuWithItems:
            [self addButton:kPLAY :@selector(playGame) :0.0f :0.0f],
            [self addButton:kHIGHSCORE :@selector(goToHighscoreScreen) :0.0f :-50.0f],
            [self addButton:kHELP :@selector(goToHelpScreen) :0.0f :-100.0f],
            [self addButton:kSOUND :@selector(adjustSound) :(SCREEN_WIDTH() / -2.0f + 70.0f) : (SCREEN_HEIGHT() / -2.0f + 70.0f)],
            nil
        ];

        [self addChild:menu];
    }

    return self;
}

-(CCMenuItemSprite*)addButton:(NSString*)sprite :(SEL)selector :(float)positionX :(float)positionY
{
    CCMenuItemSprite *button = [CCMenuItemSprite
        itemWithNormalSprite:[CCSprite spriteWithFile:sprite]
        selectedSprite:[CCSprite spriteWithFile:sprite]
        target:self selector:selector
    ];

    button.position = ccp(positionX, positionY);

    return button;
}

-(void)playGame
{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[GameScreen scene]]];
}

-(void)goToHighscoreScreen
{
}

-(void)goToHelpScreen
{
}

-(void)adjustSound
{
    
}

@end
