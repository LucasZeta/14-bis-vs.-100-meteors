//
//  TitleScreen.h
//  Bis
//
//  Created by Lucas de Souza da Conceição on 24/08/13.
//  Copyright 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface TitleScreen : CCLayer

+(CCScene *)scene;

@end
