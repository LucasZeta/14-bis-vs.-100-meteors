//
//  CCMenuItemGameButton.m
//  Bis
//
//  Created by Lucas de Souza da Conceição on 22/09/13.
//  Copyright (c) 2013 Lucas de Souza da Conceição. All rights reserved.
//
//  Button that trigger the action as it is being pressed.

#import "CCMenuItemGameButton.h"

@implementation CCMenuItemGameButton

-(void)selected
{
    [super activate];
}

-(void)activate
{
}
@end
