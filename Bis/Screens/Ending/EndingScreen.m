//
//  EndingScreen.m
//  Bis
//
//  Created by Lucas de Souza da Conceição on 20/10/13.
//  Copyright (c) 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import "EndingScreen.h"

@implementation EndingScreen

+(CCScene *)goodScene
{
    return [EndingScreen scene:YES];
}

+(CCScene *)badScene
{
    return [EndingScreen scene:NO];
}

+(CCScene *)scene:(BOOL)gameWasWon
{
    CCScene *scene = [CCScene node];
    EndingScreen *layer = [EndingScreen node];

    [layer addLabel:gameWasWon];
    [scene addChild:layer];

    return scene;
}

-(id)init
{
    self = [super init];
    
    if (self) {
        [self addBackground];
        [self addRestartButton];
    }

    return self;
}

-(void)addBackground
{
    CCSprite *sprite = [CCSprite spriteWithFile:kBACKGROUND];
    sprite.position = ccp(SCREEN_WIDTH() / 2.0f, SCREEN_HEIGHT() / 2.0f);
    
    [self addChild:sprite];
}

-(void)addLabel:(bool)gameWasWon
{
    CCSprite *sprite = [CCSprite spriteWithFile:gameWasWon ? kGAMEWON : kGAMEOVER];
    sprite.position = ccp(SCREEN_WIDTH() / 2.0f, SCREEN_HEIGHT() - 130.0f);

    [self addChild:sprite];
}

-(void)addRestartButton
{
    CCSprite *sprite1 = [CCSprite spriteWithFile:kPLAY];
    CCSprite *sprite2 = [CCSprite spriteWithFile:kPLAY];

    CCMenuItemSprite *button = [CCMenuItemSprite itemWithNormalSprite:sprite1 selectedSprite:sprite2 target:self selector:@selector(restartGame)];

    button.position = ccp(0.0f, 0.0f);
    
    CCMenu *menu = [CCMenu menuWithItems:button, nil];
    [self addChild:menu];
}

-(void)restartGame
{
    [[CCDirector sharedDirector] replaceScene:[GameScreen scene]];
}

@end
