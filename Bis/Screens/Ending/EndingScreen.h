//
//  EndingScreen.h
//  Bis
//
//  Created by Lucas de Souza da Conceição on 20/10/13.
//  Copyright (c) 2013 Lucas de Souza da Conceição. All rights reserved.
//

#import "CCLayer.h"
#import "GameScreen.h"

@interface EndingScreen : CCLayer

+(CCScene *)goodScene;
+(CCScene *)badScene;
+(CCScene *)scene:(BOOL)gameWasWon;

@end
